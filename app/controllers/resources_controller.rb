class ResourcesController < ApplicationController

  before_action :authenticate_user!

  def index
    @resources = current_user.resources.paginate(:page => params[:page], :per_page => 20)
  end

  def new
    @resource = Resource.new
  end

  def create

    allow_comma(resource_params[:value])

    @resource = Resource.new(resource_params)
    if current_user.resources << @resource
      flash[:success] = "Pomyślnie dodano zasób!"
      redirect_to resources_index_path
    else
        flash[:alert] = "Nieprawidłowe dane!"
        render :new
    end
  end

  def show
    begin
      @resource = current_user.resources.find(params[:format])
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie jesteś właścicielem tego zasobu!"
      redirect_to resources_index_path
    end
  end

  def edit
    begin
      @resource = current_user.resources.find(params[:format])
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie jesteś właścicielem tego zasobu!"
      redirect_to resources_index_path
    end
  end

  def update
    begin

      allow_comma(resource_params[:value])

      @resource = current_user.resources.find(params[:format])
      if @resource.update(resource_params)
        flash[:success] = "Pomyślnie zaktualizowano zasób!"
        redirect_to resources_index_path
      else
        flash[:alert] = "Nieprawidłowe dane!"
        render :edit
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie jesteś właścicielem tego zasobu!"
      redirect_to resources_index_path
    end
  end

  def destroy
    begin
      @resource = current_user.resources.find(params[:format])
      if current_user.resources.size > 1
        @resource.destroy
        flash[:success] = "Pomyślnie usunięto zasób!"
      else
        flash[:alert] = "Nie możesz usunąć jedynego zasobu!"
      end
      redirect_to resources_index_path
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie jesteś właścicielem tego zasobu!"
      redirect_to resources_index_path
    end
  end

  private
  def resource_params
    params.require(:resource).permit(:name, :value)
  end

  def allow_comma(number_string)
    number_string.sub!(",", ".")
  end
end
