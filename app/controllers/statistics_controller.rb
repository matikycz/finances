class StatisticsController < ApplicationController

  before_action :authenticate_user!

  def index
  	@transactions = current_user.transactions.where("shoppingTime >= :date", date: Date.today.beginning_of_month)
  end

  def categories
  	# @transactions = current_user.transactions.dateSearch(4)
  	@transactions = current_user.transactions.where("shoppingTime >= :date", date: Date.today.beginning_of_month)
  end

  def resources
  	@transactions = current_user.transactions.where("shoppingTime >= :date", date: Date.today.beginning_of_month)
  	@resources = Array.new
  	current_user.resources.each { |resource|
	  	hash = Hash.new
	  	currentValue = resource.value
	  	Date.today.downto(Date.today.beginning_of_month).each { |day|
	  		hash[Date.parse(day.to_s)] = currentValue
	  		currentValue += current_user.transactions.where("DATE(shoppingTime) = :date AND type_id = :type AND resource_id = :resource", date: day, type: 2, resource: resource.id).sum(:value)
	  		currentValue -= current_user.transactions.where("DATE(shoppingTime) = :date AND type_id = :type AND resource_id = :resource", date: day, type: 1, resource: resource.id).sum(:value)
	  	}
	  	@resources.push({name: resource.name, data: hash})
  	}
  	puts @resources
  end
end
