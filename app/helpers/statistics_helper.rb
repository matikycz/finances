module StatisticsHelper
	def monthName(month)
		case month.to_i
			when 1
				"styczeń"
			when 2
				"luty"
			when 3
				"marzec"
			when 4
				"kwiecień"
			when 5
				"maj"
			when 6
				"czerwiec"
			when 7
				"lipiec"
			when 8
				"sierpień"
			when 9
				"wrzesień"
			when 10
				"październik"
			when 11
				"listopad"
			when 12
				"grudzień"
			else
				""
		end
	end

	def tdcolor(value)
    case
      when value > 0
        "success"   # Green
      when value < 0
        "danger"    # Red
      else
        "warning"   # Yellow
    end
  end
end
