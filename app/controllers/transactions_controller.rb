class TransactionsController < ApplicationController

  before_action :authenticate_user!
  helper_method :sort_column, :sort_direction

  def index
      @transactions = current_user.transactions
        .search(params[:search])
        .dateSearch(date_search)
        .categorySearch(category_search)
        .resourceSearch(resource_search)
        .paymentSearch(payment_search)
        .typeSearch(type_search)
        .order((sort_column.empty? ? "id" : sort_column) + ' ' + sort_direction)
        .paginate(:page => params[:page], :per_page => 20)
  end

  def new
    @transaction = Transaction.new
  end

  def create
    begin
      @transaction = Transaction.new(correct_transaction_params)
      if current_user.transactions << @transaction
        resource = correct_transaction_params[:resource]
        if transaction_params[:type] == 1.to_s
          resource.value += correct_transaction_params[:value].to_f
        elsif transaction_params[:type] == 2.to_s
          resource.value -= correct_transaction_params[:value].to_f
        end
        resource.save

        flash[:success] = "Pomyślnie dodano transakcję!"
        redirect_to transactions_index_path
      else
        flash[:alert] = "Nieprawidłowe dane!"
        render :new
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nieprawidłowe dane!"
      render :new
    end
  end

  def show
    begin
      @transaction = current_user.transactions.find(params[:format])
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie jesteś właścicielem tej transakcji!"
      redirect_to transactions_index_path
    end
  end

  def edit
    begin
      @transaction = current_user.transactions.find(params[:format])
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie jesteś właścicielem tej transakcji!"
      redirect_to transactions_index_path
    end
  end

  def update
    begin
      allow_comma(transaction_params[:value])
      @transaction = current_user.transactions.find(params[:format])
      @transaction.name = transaction_params[:name]
      @transaction.value = transaction_params[:value]
      @transaction.shoppingTime = transaction_params[:shoppingTime]
      @transaction.description = transaction_params[:description]
      @transaction.payment = Payment.find(transaction_params[:payment].to_i)
      @transaction.type = Type.find(transaction_params[:type].to_i)
      @transaction.resource = current_user.resources.find(transaction_params[:resource].to_i)
      @transaction.category = current_user.categories.find(transaction_params[:category].to_i)
      if @transaction.save
        flash[:success] = "Pomyślnie zaktualizowano transakcję!"
        redirect_to transactions_index_path
      else
        flash[:alert] = "Nieprawidłowe dane!"
        render :new
      end
      rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nieprawidłowe dane!"
      redirect_to transactions_index_path
    end
  end

  def destroy
    begin
      @transaction = current_user.transactions.find(params[:format])
      @transaction.destroy
      flash[:success] = "Pomyślnie usunięto transakcję!"
      redirect_to transactions_index_path
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie jesteś właścicielem tej transakcji!"
      redirect_to transactions_index_path
    end
  end

  private
  def transaction_params
    params.require(:transaction).permit(:name, :value, :shoppingTime, :description, :payment, :type, :resource, :category)
  end

  def allow_comma(number_string)
    number_string.sub(",", ".")
  end

  def correct_transaction_params
    corret = Hash.new(8)
    corret[:name] = transaction_params[:name]
    corret[:value] = allow_comma(transaction_params[:value]) unless transaction_params[:value].nil?
    corret[:shoppingTime] = transaction_params[:shoppingTime]
    corret[:description] = transaction_params[:description]
    corret[:payment] = Payment.find(transaction_params[:payment].to_i)
    corret[:type] = Type.find(transaction_params[:type].to_i)
    corret[:resource] = current_user.resources.find(transaction_params[:resource].to_i)
    corret[:category] = current_user.categories.find(transaction_params[:category].to_i)
    corret
  end

  def sort_column
    Transaction.column_names.include?(params[:sort]) ? params[:sort] : ""
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end

  def date_search
    params[:date].present? ? params[:date].to_i : 0
  end

  def category_search
    params[:category].present? ? params[:category].to_i : 0
  end

  def resource_search
    params[:resource].present? ? params[:resource].to_i : 0
  end

  def payment_search
    params[:payment].present? ? params[:payment].to_i : 0
  end

  def type_search
    params[:type].present? ? params[:type].to_i : 0
  end
end
