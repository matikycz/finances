class Resource < ActiveRecord::Base
  belongs_to :user
  has_many :transactions

  validates :name, presence: true
  validates :value, presence: true, numericality: true
  validates :user, presence: true
end
