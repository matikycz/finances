$(document).on 'page:load ready', ->
	$('#datetimepicker').datetimepicker locale: 'pl', defaultDate: new Date

	$('#descriptionlabel').click ->
		if $('#transaction_description').css('display') != 'none'
			$('#transaction_description').hide()
			$('#descriptionlabel').text("(Pokaż)")
		else
			$('#transaction_description').show()
			$('#descriptionlabel').text("(Ukryj)")

	$('#transactions_search').submit ->
		$.get(this.action, $(this).serialize(), null, 'script');
		false

	$('#transactions_search input').keyup ->
		$.get($("#transactions_search").attr("action"), $("#transactions_search").serialize(), null, 'script');
		false

	$('#datesearch, #categorysearch, #resourcesearch, #paymentsearch, #typesearch').change ->
		$.get($("#transactions_search").attr("action"), $("#transactions_search").serialize(), null, 'script');
		false

$(document).on 'click', '.sortinglink, #transactions .pagination a', (e) ->
	jQuery ($) ->
		$.getScript e.currentTarget.href
	false