class Transaction < ActiveRecord::Base
  belongs_to :user
  belongs_to :resource
  belongs_to :category
  belongs_to :payment
  belongs_to :type

  validates :name, presence: true
  validates :value, presence: true, numericality: { greater_than: 0 }
  validates :shoppingTime, presence: true
  validates :user, presence: true
  validates :resource, presence: true
  validates :category, presence: true
  validates :payment, presence: true
  validates :type, presence: true

  def self.search(search)
  	if search
  		where("name LIKE :query or description LIKE :query",query: "%#{search}%")
  	else
		all
	end
  end

  def self.dateSearch(date)
  	case date.to_i
  		when 0
  			all
  		when 1
  			where("DATE(shoppingTime) = :date", date: Date.today)
  		when 2
  			where("DATE(shoppingTime) >= :date", date: 1.week.ago)
  		when 3
  			where("DATE(shoppingTime) >= :date", date: 2.week.ago)
  		when 4
  			where("DATE(shoppingTime) >= :date", date: 1.month.ago)
  		when 5
  			where("DATE(shoppingTime) >= :date", date: 3.month.ago)
  		when 6
  			where("DATE(shoppingTime) >= :date", date: 6.month.ago)
  		when 7
  			where("DATE(shoppingTime) >= :date", date: 1.year.ago)
  		else
  			none
  	end
  end

  def self.categorySearch(category)
  	case category.to_i
  		when 0
  			all
  		else
  			where("category_id = #{category}")
  	end
  end

  def self.resourceSearch(resource)
  	case resource.to_i
  		when 0
  			all
  		else
  			where("resource_id = #{resource}")
  	end
  end

  def self.paymentSearch(payment)
  	case payment.to_i
  		when 0
  			all
  		else
  			where("payment_id = #{payment}")
  	end
  end

  def self.typeSearch(type)
  	case type.to_i
  		when 0
  			all
  		else
  			where("type_id = #{type}")
  	end
  end
end
