class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.string :name, null: false
      t.float :value, null: false
      t.datetime :shoppingTime, null: false
      t.text :description
      t.references :user, index: true, foreign_key: true, null: false
      t.references :resource, index: true, foreign_key: true, null: false
      t.references :category, index: true, foreign_key: true, null: false
      t.references :payment, index: true, foreign_key: true, null: false
      t.references :type, index: true, foreign_key: true, null: false

      t.timestamps null: false
    end
  end
end
