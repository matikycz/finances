# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150528201434) do

  create_table "categories", force: :cascade do |t|
    t.string   "name",       limit: 255, null: false
    t.integer  "user_id",    limit: 4,   null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "categories", ["user_id"], name: "index_categories_on_user_id", using: :btree

  create_table "payments", force: :cascade do |t|
    t.string   "name",       limit: 255, null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "resources", force: :cascade do |t|
    t.string   "name",       limit: 255, null: false
    t.float    "value",      limit: 24,  null: false
    t.integer  "user_id",    limit: 4,   null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "resources", ["user_id"], name: "index_resources_on_user_id", using: :btree

  create_table "transactions", force: :cascade do |t|
    t.string   "name",         limit: 255,   null: false
    t.float    "value",        limit: 24,    null: false
    t.datetime "shoppingTime",               null: false
    t.text     "description",  limit: 65535
    t.integer  "user_id",      limit: 4,     null: false
    t.integer  "resource_id",  limit: 4,     null: false
    t.integer  "category_id",  limit: 4,     null: false
    t.integer  "payment_id",   limit: 4,     null: false
    t.integer  "type_id",      limit: 4,     null: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "transactions", ["category_id"], name: "index_transactions_on_category_id", using: :btree
  add_index "transactions", ["payment_id"], name: "index_transactions_on_payment_id", using: :btree
  add_index "transactions", ["resource_id"], name: "index_transactions_on_resource_id", using: :btree
  add_index "transactions", ["type_id"], name: "index_transactions_on_type_id", using: :btree
  add_index "transactions", ["user_id"], name: "index_transactions_on_user_id", using: :btree

  create_table "types", force: :cascade do |t|
    t.string   "name",       limit: 255, null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "login",                  limit: 255,              null: false
    t.string   "name",                   limit: 255,              null: false
    t.string   "surname",                limit: 255,              null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "categories", "users"
  add_foreign_key "resources", "users"
  add_foreign_key "transactions", "categories"
  add_foreign_key "transactions", "payments"
  add_foreign_key "transactions", "resources"
  add_foreign_key "transactions", "types"
  add_foreign_key "transactions", "users"
end
