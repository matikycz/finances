class CategoriesController < ApplicationController

  before_action :authenticate_user!

  def index
    @categories = current_user.categories.paginate(:page => params[:page], :per_page => 20)
  end

  def new
    @category = Category.new
  end

  def create
    @category = Category.new(category_params)
    if current_user.categories << @category
      flash[:success] = "Pomyślnie dodano kategorię!"
      redirect_to categories_index_path
    else
      flash[:alert] = "Nieprawidłowe dane!"
      render :new
    end
  end

  def show
    begin
      @category = current_user.categories.find(params[:format])
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie jesteś właścicielem tej kategorii!"
      redirect_to categories_index_path
    end
  end

  def edit
    begin
      @category = current_user.categories.find(params[:format])
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie jesteś właścicielem tej kategorii!"
      redirect_to categories_index_path
    end
  end

  def update
    begin
      @category = current_user.categories.find(params[:format])
      if @category.update(category_params)
        flash[:success] = "Pomyślnie zaktualizowano kategorię!"
        redirect_to categories_index_path
      else
        flash[:alert] = "Nieprawidłowe dane!"
        render :edit
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie jesteś właścicielem tej kategorii!"
      redirect_to categories_index_path
    end
  end

  def destroy
    begin
      @category = current_user.categories.find(params[:format])
      if current_user.categories.size > 1
        @category.destroy
        flash[:success] = "Pomyślnie usunięto kategorię!"
      else
        flash[:alert] = "Nie możesz usunąć jedynej kategorii!"
      end
      redirect_to categories_index_path
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie jesteś właścicielem tej kategorii!"
      redirect_to categories_index_path
    end
  end

  private
  def category_params
    params.require(:category).permit(:name)
  end
end