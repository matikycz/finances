Rails.application.routes.draw do

  root 'home#index'

  if Rails.env.development?
    mount LetterOpenerWeb::Engine, at: '/emails'
  end

  get 'home/index'

  get 'resources/index'
  get 'resources/new'
  post 'resources/create'
  get 'resources/show'
  get 'resources/edit'
  put 'resources/update'
  delete 'resources/destroy'

  get 'categories/index'
  get 'categories/new'
  post 'categories/create'
  get 'categories/show'
  get 'categories/edit'
  put 'categories/update'
  delete 'categories/destroy'


  get 'transactions/index'
  get 'transactions/new'
  post 'transactions/create'
  get 'transactions/show'
  get 'transactions/edit'
  put 'transactions/update'
  delete 'transactions/destroy'

  get 'statistics/index'
  get 'statistics/categories'
  get 'statistics/resources'

  # devise_for :users

  devise_for :users, controllers: {
        # sessions: 'sessions',
        registrations: 'users/registrations'
      }

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
